#The script make_quick_narrowbandimages.py allow to make narrow band images from a MUSE cube. 
#mpdaf is necessary to run the script

#3 things are mandatory to run the script, a MUSE cube, a the position in the sky (RA,DEC), and wavelength (ideally an emmission line)
#The ID is, you need to set your narrow band spatially and spectrally,

#Spectrally the narrow band images will  works like this
#                                               lambda                                                    option -l
#                       | lambda -1/2 wave_win           lambda +1/2 wave_win |                           option -m
#                || lambda -1/2 mask (to exclude from continuum )  lambda +1/2 mask ||                    option -w
#||| continuum   ||                                                                 ||  continuum  |||    option -s

#Spacially, you specify the postion RA,DEC  
#                and by default it will cut the a square FOV of 20 arcsec aside.
#          You could change it with the -f option 


#The script compute a MAD statistics on the image and give you the 1sigma of your image.

#The script produce a spectra from a square aperture of 8 arcsec centered on the position you gave,
#     it could help to refine the boudaries of your narrowband images.

#I list bellow all this option as they apperaed if you do python make_quick_narrowbandimages.py --help
#Options:
#  -h, --help            show this help message and exit
#  -s SPEC_RANGE, --spectral_continuum_range=SPEC_RANGE
#                        spectral range where you will have the contnuum
#                        suroundings
#  -f FOV, --spatial_fov=FOV
#                        spatial window in arcsec, value correspond to the
#                        delta axis and image is a square
#  -w WAVE_WIN, --wavelength_window=WAVE_WIN
#                        wavelength window, full range centered on the line
#                        selected
#  -l LMBDA, --lambda=LMBDA
#                        lambda
#  -c CUBE, --cube=CUBE  cube
#  -o NAME, --output=NAME
#                        output name of the narrow_band images
#  -m MASK, --mask_continuum=MASK
#                        size of the mask in pixel for the central part of the
#                        spectrum  ||continuum | mask | continuum ||
#
