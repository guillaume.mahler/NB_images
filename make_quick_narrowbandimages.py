from mpdaf.obj import Cube,Image,Spectrum,iter_ima #Iter_ima va permettre de parcourir tous les plans(longueur d'onde) les un apres les autres
import numpy as np
#import read_master_table as rmt
#import read_ray_wave as rrw
import os
import optparse
import sys
import scipy

def mad(arr):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
    med = np.median(arr)

    return np.median(np.abs(arr - med))


def make_minicube(c,DEC,RA,arcsec_size,ray_lmd,agstr_window=110):
	minic=c.subcube((DEC,RA),arcsec_size,lbda=(ray_lmd-agstr_window/2.0,ray_lmd+agstr_window/2.0))
	return minic
	
def make_continuum_free_cube(mc,size_box=5):
	mc_res=mc.copy()
	pixmidlambda=len(mc.data.data[:,0,0])/2
	for yy in range(0,len(mc.data.data[0,:,0])):
        	for xx in range(0,len(mc.data.data[0,0,:])):
                	#avepix=np.mean([np.mean(mc.data.data[:pixmidlambda-size_box,yy,xx]),np.mean(mc.data.data[pixmidlambda+size_box:,yy,xx])  ])  #DO the mean of the mean
                	#med_pix=np.mean([np.median(mc.data.data[:pixmidlambda-size_box,yy,xx]),np.median(mc.data.data[pixmidlambda+size_box:,yy,xx])  ])  #DO the mean of the median
                	#mad_pix=mad(np.concatenate((mc.data.data[:pixmidlambda-size_box,yy,xx],mc.data.data[pixmidlambda+size_box:,yy,xx])))              
			sgmcl_pix=np.mean(scipy.stats.sigmaclip(np.concatenate((mc.data.data[:pixmidlambda-size_box,yy,xx],mc.data.data[pixmidlambda+size_box:,yy,xx])),7,7)[0]) #sigma clipping
			#mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-avepix
			#mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-mad_pix
			#mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-med_pix
			mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-sgmcl_pix
	return mc_res


def get_first_sigma(im):
        return 1.4855*mad(im.data.data)

def process_cube_to_get_narrow_band_images_of_multiple(c,RA,DEC,wavelength,space_window,ag_win,lmbda_range,size_box,NB_outname):
	minic=make_minicube(c,DEC,RA,space_window,wavelength,agstr_window=ag_win)
	mc_wc=make_continuum_free_cube(minic,size_box)		
	mc_narrow=make_minicube(mc_wc,DEC,RA,space_window,wavelength,agstr_window=lmbda_range)
        
        im=mc_narrow.sum(axis=0)
	print 'the first sigma level according to a mad statistic (1.4855*mad) of the picture is',get_first_sigma(im)
        im.write(NB_outname)
        
	test=mc_wc.subcube((DEC,RA),size=8)   # I male spectra from a smaller region
        spec=test.sum(axis=(1,2))
	np.savetxt('spec_output.txt',zip(test.wave.coord(),spec.data.data))
        


if __name__== "__main__":
	

	parser = optparse.OptionParser()
	parser.add_option('-s', '--spectral_continuum_range', dest='spec_range', help='spectral range where you will have the contnuum suroundings',type=float)
	parser.add_option('-f', '--spatial_fov', dest='fov', help='spatial window in arcsec, value correspond to the delta axis and image is a square',type=float)
	parser.add_option('-w', '--wavelength_window', dest='wave_win', help='wavelength window, full range centered on the line selected',type=float)
	parser.add_option('-l', '--lambda', dest='lmbda', help='lambda',type=float)
	parser.add_option('-c', '--cube', dest='cube', help='cube')
	parser.add_option('-o', '--output', dest='name', help='output name of the narrow_band images')
	parser.add_option('-m', '--mask_continuum', dest='mask', help='size of the mask in pixel for the central part of the spectrum  ||continuum | mask | continuum ||',type=float)
	(options, args) = parser.parse_args()
	try:
		RADEC=sys.argv[1]
		RA=np.float(RADEC.split(',')[0])
		DEC=np.float(RADEC.split(',')[1])
	except:
		RADEC=str(raw_input('Enter RA,DEC:'))
		RA=np.float(RADEC.split(',')[0])
                DEC=np.float(RADEC.split(',')[1])

	#cube_default_path='/data/guillaume/A2744_data/ZAP/DATACUBE_A2744_ZAP_1Jan_MAD_ZAP_median.fits'
	cube_default_path='/data/guillaume/A2744_data/ZAP/DATACUBE_A2744_ZAP_MAD_ZAP_Median.fits'
	#cube_default_path='/data/guillaume/A2744_data/CFCS/DATACUBE_A2744_CubeFixSharp_20Dec_1_MAD.fits'

	if options.spec_range is None:
		agstr_window=110
		print 'default spectral range =',agstr_window
	else:
		agstr_window=options.spec_range
		print 'user define spectral range =',agstr_window
	if options.mask is None:
		size_box=5
	else:
		size_box=options.mask/2.0
	if options.wave_win is None:
		lmbda_range=10
	        print 'default wavelength_window = ',lmbda_range
	else:
		lmbda_range=options.wave_win
	        print 'user set wavelength_window =',lmbda_range

	if options.lmbda is None:
		options.lmbda= raw_input('Enter lambda:')
		wavelength=float(options.lmbda)
	else:	
		wavelength=options.lmbda
		print 'option lamba= ',wavelength

	if options.cube is None:
		try:
			c=Cube(cube_default_path)
		except:
			options.cube=raw_input('Enter Cube path:')
			c=Cube(str(options.cube))
	else:
		c=Cube(str(options.cube))

	if options.name is None:
		NB_outname='narrowbandimages_output.fits'
	else :
		NB_outname=options.name

	if options.fov is None:
                space_window=20.0
        else :
                space_window=options.fov

	
	print size_box
	process_cube_to_get_narrow_band_images_of_multiple(c,RA,DEC,wavelength,space_window,agstr_window,lmbda_range,size_box,NB_outname)



